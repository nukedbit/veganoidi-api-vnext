﻿/*    Copyright 2015 Sebastian Faltoni
 *
 *    This program is free software: you can redistribute it and/or  modify
 *    it under the terms of the GNU Affero General Public License, version 3,
 *    as published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    As a special exception, the copyright holders give permission to link the
 *    code of portions of this program with the OpenSSL library under certain
 *    conditions as described in each individual source file and distribute
 *    linked combinations including the program with the OpenSSL library. You
 *    must comply with the GNU Affero General Public License in all respects
 *    for all of the code used other than as permitted herein. If you modify
 *    file(s) with this exception, you may extend this exception to your
 *    version of the file(s), but you are not obligated to do so. If you do not
 *    wish to do so, delete this exception statement from your version. If you
 *    delete this exception statement from all source files in the program,
 *    then also delete it in the license file.
 */

using System.Linq;
using System.Threading.Tasks;
using NukeCms.Api.Documents;
using NukeCms.Api.Domain.Search;
using NUnit.Framework;
using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Embedded;

namespace NukeCms.Api.Tests
{
    [TestFixture]
    public class SearchBlogPosts
    {
        [Test]
        public async Task SearchPostsByCategoryReturnsOne()
        {
            const int expectedDocumentCount = 1;
            const int expectedPageIndex = 0;
            const int expectedPageCount = 1;
            const string expectedCategoryId = "categoryid";
            const string notExpectedCategoryId = "notexpectedid";
            using (var documentStore = CreateDocumentStore())
            {
                await new BlogPostSearchByCategoryIndexBuilder(documentStore).Build();

                using (var session = documentStore.OpenAsyncSession())
                {

                    var document = CreateDocumentWithOneCategoryId(expectedCategoryId);
                    var document2 = CreateDocumentWithOneCategoryId(notExpectedCategoryId);

                    await StoreDocuments(session, document, document2);

                    var searchPostEngine = new SearchPostEngine(session);

                    var result = await searchPostEngine.Search(new ProductInCategoryRequest(expectedCategoryId, expectedPageIndex, 10));

                    Assert.AreEqual(expectedDocumentCount, result.Documents.Count);
                    var resultCategoryOfDocument = result.Documents.First().Categories.First();
                    Assert.AreEqual(resultCategoryOfDocument.Id, expectedCategoryId);
                    Assert.AreEqual(expectedPageIndex, result.GetPageIndex());
                    Assert.AreEqual(expectedPageCount, result.GetPageCount());
                }
            }
        }


        [TestCase(3, 2, 2, 1)]
        [TestCase(4, 2, 2, 2)]
        public async Task SearchPostsWithPagination(int documentsCount, int pageSize,
            int pageOneExpectedCount, int pageTwoExpectedCount)
        {
            const int expectedPageCount = 2;
            const string expectedCategoryId = "expectedCategoryId";

            using (var documentStore = CreateDocumentStore())
            {
                await new BlogPostSearchByCategoryIndexBuilder(documentStore).Build();

                using (var session = documentStore.OpenAsyncSession())
                {
                    for (int i = 0; i < documentsCount; i++)
                    {
                        var document = CreateDocumentWithOneCategoryId(expectedCategoryId);
                        await StoreDocuments(session, document);
                    }

                    var searchPostEngine = new SearchPostEngine(session);

                    var pageIndex = 0;
                    var result = await searchPostEngine.Search(new ProductInCategoryRequest(expectedCategoryId, pageIndex, pageSize));

                    Assert.AreEqual(pageOneExpectedCount, result.Documents.Count);
                    Assert.AreEqual(pageIndex, result.GetPageIndex());
                    Assert.AreEqual(expectedPageCount, result.GetPageCount());

                    pageIndex++;
                    result = await searchPostEngine.Search(new ProductInCategoryRequest(expectedCategoryId, pageIndex, pageSize));

                    Assert.AreEqual(pageTwoExpectedCount, result.Documents.Count);
                    Assert.AreEqual(pageIndex, result.GetPageIndex());
                    Assert.AreEqual(2, result.GetPageCount());
                }
            }
        }

        [Test]
        public async Task SearchOnContentFindOne()
        {
            using (var documentStore = CreateDocumentStore())
            {
                await new BlogPostTextSearchIndexBuilder(documentStore).Build();
                using (var session = documentStore.OpenAsyncSession())
                {
                    const string expectedDocumentId = "my-document-id";
                    const string expectedContent = "this blog post is blue, and the title is color red";
                    var blogDocument = new BlogPostDocument()
                    {
                        Id = expectedDocumentId,
                        Content = expectedContent
                    };
                    var blogDocument2 = new BlogPostDocument()
                    {
                        Id = "document2",
                        Content = "content is two"
                    };
                    await StoreDocuments(session, blogDocument, blogDocument2);

                    var searchPostEngine = new SearchPostEngine(session);
                    var result = await searchPostEngine.Search(new TextSearchRequest("color blue", 0, 2));

                    Assert.AreEqual(1, result.Documents.Count);

                    Assert.AreEqual(expectedDocumentId, result.Documents.First().Id);
                }
            }
        }

        private static async Task StoreDocuments(IAsyncDocumentSession session, params BlogPostDocument[] docs)
        {
            foreach (var document in docs)
            {
                await session.StoreAsync(document);
            }
            await session.SaveChangesAsync();
        }

        private static BlogPostDocument CreateDocumentWithOneCategoryId(string expectedCategoryId)
        {
            return new BlogPostDocument()
            {
                Title = "title",
                Categories = new[]
                {
                    new CategoryDocument(expectedCategoryId,"not usefull" ,null),
                }
            };
        }

        private IDocumentStore CreateDocumentStore()
        {
            var documentStore = new EmbeddableDocumentStore
            {
                RunInMemory = true,
                Conventions =
                {
                    DefaultQueryingConsistency = ConsistencyOptions.AlwaysWaitForNonStaleResultsAsOfLastWrite
                }
            };
            documentStore.Initialize();
            return documentStore;
        }
    }
}
