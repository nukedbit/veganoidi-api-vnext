﻿/*    Copyright 2015 Sebastian Faltoni
 *
 *    This program is free software: you can redistribute it and/or  modify
 *    it under the terms of the GNU Affero General Public License, version 3,
 *    as published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    As a special exception, the copyright holders give permission to link the
 *    code of portions of this program with the OpenSSL library under certain
 *    conditions as described in each individual source file and distribute
 *    linked combinations including the program with the OpenSSL library. You
 *    must comply with the GNU Affero General Public License in all respects
 *    for all of the code used other than as permitted herein. If you modify
 *    file(s) with this exception, you may extend this exception to your
 *    version of the file(s), but you are not obligated to do so. If you do not
 *    wish to do so, delete this exception statement from your version. If you
 *    delete this exception statement from all source files in the program,
 *    then also delete it in the license file.
 */

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using NukeCms.Api.Documents;
using NukeCms.Api.Readers;
using NUnit.Framework;
using Raven.Client;
using Raven.Client.Embedded;

namespace NukeCms.Api.Tests
{
	public class RecentBlogPostsTests
	{
		[TestCase(5)]
		[TestCase(10)]
		public async Task RecentBlogPostsFive(int postsCount)
		{
			using (var documentStore = CreateDocumentStore())
			{
				using (var session = documentStore.OpenAsyncSession())
				{
					var creationTime = new DateTime(2015,1,1);
					await PrepareDocumentSessionWithRecentBlogPosts(postsCount, session,creationTime);
					var reader = new RecentBlogPostsReader(session);
					var recents = await reader.GetAsync();

					Assert.AreEqual(postsCount, recents.Count());
				}
			}
		}

		private IDocumentStore CreateDocumentStore()
		{
			var documentStore = new EmbeddableDocumentStore { RunInMemory = true };
			documentStore.Initialize();
			return documentStore;
		}

		private async Task PrepareDocumentSessionWithRecentBlogPosts(int postsCount, IAsyncDocumentSession session, DateTime creationTime)
		{
			var blogPosts = new List<BlogPostDocument>();
			for (var i = 0; i < postsCount; i++)
			{
				blogPosts.Add(new BlogPostDocument());
			}
			await session.StoreAsync(new RecentBlogPostsDocument(blogPosts, creationTime));
			await session.SaveChangesAsync();
		}


		[Test]
		public async Task RecentBlogPostsGetRecentDocument()
		{
			using (var documentStore = CreateDocumentStore())
			{
				using (var session = documentStore.OpenAsyncSession())
				{
					var oldCreationTime = new DateTime(2015, 1, 1);
					var recentCreationTime = new DateTime(2015, 1, 2);

					await PrepareDocumentSessionWithRecentBlogPosts(2, session, oldCreationTime);
					await PrepareDocumentSessionWithRecentBlogPosts(5, session, recentCreationTime);

					var reader = new RecentBlogPostsReader(session);
					var recents = await reader.GetAsync();

					Assert.AreEqual(5, recents.Count());
				}
			}
		}		
	}
}
