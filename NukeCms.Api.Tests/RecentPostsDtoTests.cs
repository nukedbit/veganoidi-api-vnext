﻿/*    Copyright 2015 Sebastian Faltoni
 *
 *    This program is free software: you can redistribute it and/or  modify
 *    it under the terms of the GNU Affero General Public License, version 3,
 *    as published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    As a special exception, the copyright holders give permission to link the
 *    code of portions of this program with the OpenSSL library under certain
 *    conditions as described in each individual source file and distribute
 *    linked combinations including the program with the OpenSSL library. You
 *    must comply with the GNU Affero General Public License in all respects
 *    for all of the code used other than as permitted herein. If you modify
 *    file(s) with this exception, you may extend this exception to your
 *    version of the file(s), but you are not obligated to do so. If you do not
 *    wish to do so, delete this exception statement from your version. If you
 *    delete this exception statement from all source files in the program,
 *    then also delete it in the license file.
 */

using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using NukeCms.Api.Documents;
using NukeCms.Api.Domain;
using NUnit.Framework;

namespace NukeCms.Api.Tests
{
	[TestFixture]
	public class RecentPostsDtoTests
	{
		[Test]
		public void RecentPostsToDto()
		{
			var expectedJson = GetExpectedJson();
			var documents = new List<BlogPostDocument> {
				GetTestBlogPostDocument()
			};
			var document = new RecentBlogPostsDocument(documents, new DateTime(2015, 1, 1));
			var recentPosts = new RecentPosts(document);
			var generator = recentPosts.GetDtoGenerator();
			dynamic dto = generator.Generate();

			var serializedDto = JsonConvert.SerializeObject(dto, Formatting.None);

			Assert.AreEqual(expectedJson, serializedDto);
		}

		private static BlogPostDocument GetTestBlogPostDocument()
		{
		    const string primiPiattiId = "primipiattiid";
		    const string sughiId = "sughiid";
		    return new BlogPostDocument()
			{
				Title = "a sample title",
				Content = "post content example for testing",
				CreationDate = new DateTime(2015, 1, 1),
				Tags = new[] { "primi", "pomodoro" },
				Categories = new[]{new CategoryDocument(primiPiattiId, "primi piatti",null),
					new CategoryDocument(sughiId,"sughi",null)  },
				ThumbnailUrl = "http://www.google.it",
				Authors = new List<Author>
				{
					new Author()
					{
						FullName = "author name",
						UserName = "username",
						Email = "author@email.com"
					}
				},
			};
		}

	    private static string GetExpectedJson()
		{
			var expectedJson =
				new StreamReader(typeof(RecentBlogPostsTests).Assembly
				.GetManifestResourceStream("NukeCms.Api.Tests.BlogPost.json"))
					.ReadToEnd();
			expectedJson = expectedJson.Replace("\t", "");
			expectedJson = expectedJson.Replace("\r\n", "");
			return expectedJson;
		}
	}
}
