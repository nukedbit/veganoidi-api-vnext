﻿/*    Copyright 2015 Sebastian Faltoni
 *
 *    This program is free software: you can redistribute it and/or  modify
 *    it under the terms of the GNU Affero General Public License, version 3,
 *    as published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    As a special exception, the copyright holders give permission to link the
 *    code of portions of this program with the OpenSSL library under certain
 *    conditions as described in each individual source file and distribute
 *    linked combinations including the program with the OpenSSL library. You
 *    must comply with the GNU Affero General Public License in all respects
 *    for all of the code used other than as permitted herein. If you modify
 *    file(s) with this exception, you may extend this exception to your
 *    version of the file(s), but you are not obligated to do so. If you do not
 *    wish to do so, delete this exception statement from your version. If you
 *    delete this exception statement from all source files in the program,
 *    then also delete it in the license file.
 */

using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using NukeCms.Api.Collections;
using NukeCms.Api.Documents;
using NukeCms.Api.Readers;
using NUnit.Framework;
using Raven.Client;
using Raven.Client.Embedded;

namespace NukeCms.Api.Tests
{
	[TestFixture]
	public class ReadCategoryTests
	{
		[Test]
		public async Task ReadCategories()
		{
			using (var documentStore = CreateDocumentStore())
			{
				using (var session = documentStore.OpenAsyncSession())
				{
					var primiPiattiCategory = new CategoryDocument("primipiattiid", "primi piatti", null);
					var secondiCategory = new CategoryDocument("secondipiattidid", "secondi", null);

					await StoreCategories(session, primiPiattiCategory, secondiCategory);

					var reader = new CategoryReader(session);

					var categories = await reader.GetAsync();

					Assert.IsNotNull(categories.FirstOrDefault(p => p.Id == primiPiattiCategory.Id));
					Assert.IsNotNull(categories.FirstOrDefault(p => p.Id == secondiCategory.Id));
				}
			}
		}

		[Test]
		public async Task ReadSubCategories()
		{
			using (var documentStore = CreateDocumentStore())
			{
				using (var session = documentStore.OpenAsyncSession())
				{
					var subCategory = new CategoryDocument("alsugoid", "al sugo", null);

					var primiPiattiCategory = new CategoryDocument("primipiattiid", "primi piatti",
						new ReadOnlyCollection<CategoryDocument>(
							new[] { subCategory }
						));

					await StoreCategories(session, primiPiattiCategory);

					var reader = new CategoryReader(session);

					var categories = await reader.GetAsync();

					Assert.AreEqual(subCategory.Id, categories.First().Childrens.First().Id);
				}
			}
		}

		[Test]
		public async Task ReadCategoriesInAlphabeticOrder()
		{
			using (var documentStore = CreateDocumentStore())
			{
				using (var session = documentStore.OpenAsyncSession())
				{
					var categoryB = new CategoryDocument("idb", "B", null);
					var categoryC = new CategoryDocument("idc", "C", null);
					var categoryA = new CategoryDocument("ida", "A", null);
					var categoryF = new CategoryDocument("idf", "F", null);

					await StoreCategories(session, categoryF, categoryC, categoryA, categoryB);

					var reader = new CategoryReader(session);

					var categories = await reader.GetAsync();

					Assert.AreEqual(categories.ElementAt(0).Name, categoryA.Name);
					Assert.AreEqual(categories.ElementAt(1).Name, categoryB.Name);
					Assert.AreEqual(categories.ElementAt(2).Name, categoryC.Name);
					Assert.AreEqual(categories.ElementAt(3).Name, categoryF.Name);
				}
			}
		}

		[Test]
		public void ChildrensNotNull()
		{
			var category = new CategoryDocument("id", "not usefull", null);
			Assert.IsInstanceOf(typeof(EmptyReadOnlyCollection<CategoryDocument>), category.Childrens);
		}

		private static async Task StoreCategories(IAsyncDocumentSession session, params CategoryDocument[] categories)
		{
			foreach (var document in categories)
			{
				await session.StoreAsync(document);
			}
			await session.SaveChangesAsync();
		}

		private IDocumentStore CreateDocumentStore()
		{
			var documentStore = new EmbeddableDocumentStore { RunInMemory = true };
			documentStore.Initialize();
			return documentStore;
		}
	}
}
