/*    Copyright 2015 Sebastian Faltoni
 *
 *    This program is free software: you can redistribute it and/or  modify
 *    it under the terms of the GNU Affero General Public License, version 3,
 *    as published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    As a special exception, the copyright holders give permission to link the
 *    code of portions of this program with the OpenSSL library under certain
 *    conditions as described in each individual source file and distribute
 *    linked combinations including the program with the OpenSSL library. You
 *    must comply with the GNU Affero General Public License in all respects
 *    for all of the code used other than as permitted herein. If you modify
 *    file(s) with this exception, you may extend this exception to your
 *    version of the file(s), but you are not obligated to do so. If you do not
 *    wish to do so, delete this exception statement from your version. If you
 *    delete this exception statement from all source files in the program,
 *    then also delete it in the license file.
 */

using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using NukeCms.Api.Documents;

namespace NukeCms.Api.Domain
{
	public class RecentPostsDataTransferObjectGenerator : IDataTransferObjectGenerator
	{
		private readonly RecentBlogPostsDocument _recentPosts;

		internal RecentPostsDataTransferObjectGenerator(RecentBlogPostsDocument recentPosts)
		{
			_recentPosts = recentPosts;
		}

		public dynamic Generate()
		{
			var posts = new List<ExpandoObject>();
			foreach (var post in _recentPosts.Posts)
			{
				dynamic postDto = new ExpandoObject();
				postDto.title = post.Title;
				postDto.content = post.Content;
				postDto.creation_date = post.CreationDate;
				postDto.tags = post.Tags;
				postDto.categories = post.Categories.Select(c=> GetCategoryExpandoObject(c)).ToArray();
				postDto.thumbnail_url = post.ThumbnailUrl;
				postDto.authors = post.Authors
					.Select(p=> GetAuthorExpandoObject(p));
				posts.Add(postDto);
			}
			return posts.ToArray();
		}

	    private dynamic GetCategoryExpandoObject(CategoryDocument categoryDocument)
	    {
	        dynamic category = new ExpandoObject();
	        category.id = categoryDocument.Id;
	        category.name = categoryDocument.Name;
	        category.childrens = categoryDocument.Childrens
                .Select(c => GetCategoryExpandoObject(c)).ToArray();
	        return category;
	    }

	    private dynamic GetAuthorExpandoObject(Author author)
		{
			dynamic authorDto = new ExpandoObject();
			authorDto.fullname = author.FullName;
			authorDto.username = author.UserName;
			authorDto.email = author.Email;
			return authorDto;
		}
	}
}