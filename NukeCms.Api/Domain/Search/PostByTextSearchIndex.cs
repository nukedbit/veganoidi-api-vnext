/*    Copyright 2015 Sebastian Faltoni
 *
 *    This program is free software: you can redistribute it and/or  modify
 *    it under the terms of the GNU Affero General Public License, version 3,
 *    as published by the Free Software Foundation.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *    As a special exception, the copyright holders give permission to link the
 *    code of portions of this program with the OpenSSL library under certain
 *    conditions as described in each individual source file and distribute
 *    linked combinations including the program with the OpenSSL library. You
 *    must comply with the GNU Affero General Public License in all respects
 *    for all of the code used other than as permitted herein. If you modify
 *    file(s) with this exception, you may extend this exception to your
 *    version of the file(s), but you are not obligated to do so. If you do not
 *    wish to do so, delete this exception statement from your version. If you
 *    delete this exception statement from all source files in the program,
 *    then also delete it in the license file.
 */

using System.Linq;
using NukeCms.Api.Documents;
using Raven.Abstractions.Indexing;
using Raven.Client.Indexes;

namespace NukeCms.Api.Domain.Search
{
    public class PostByTextSearchIndex : AbstractIndexCreationTask<BlogPostDocument, PostByTextSearchIndex.ReduceResult>
    {
        public PostByTextSearchIndex()
        {
            Map = posts => from post in posts
                           select new
                           {
                               post.Title,
                               post.Content
                           };
            Store(x => x.Title, FieldStorage.Yes);
            Store(x => x.Content, FieldStorage.Yes);
            this.Index(p => p.Title, FieldIndexing.Analyzed);
            this.Index(p => p.Content, FieldIndexing.Analyzed);
        }

        public class ReduceResult
        {
            public string Title { get; set; }
            public string Content { get; set; }
        }
    }
}